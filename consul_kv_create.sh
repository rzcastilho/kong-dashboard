#!/bin/bash

source /etc/environment

consul kv put $APPLICATION_NAME/consul/service/name "kong-dashboard"
consul kv put $APPLICATION_NAME/consul/service/port "8080"
consul kv put $APPLICATION_NAME/consul/service/check/id "DASHBOARD"
consul kv put $APPLICATION_NAME/consul/service/check/name "Kong Dashboard"
consul kv put $APPLICATION_NAME/consul/service/check/tcp "localhost:8080"
consul kv put $APPLICATION_NAME/consul/service/check/interval "30s"
consul kv put $APPLICATION_NAME/consul/service/check/timeout "1s"
