#!/bin/bash

set -e

source /etc/environment

# Verify if APPLICATION_NAME exists, if not exists set default value
if [ -z ${APPLICATION_NAME+x} ]; then
	APPLICATION_NAME=$APPLICATION_DEFAULT_NAME
fi
export APPLICATION_NAME

# Verify if KONG_ADMIN_PROTOCOL exists, if not exists set default value
if [ -z ${KONG_ADMIN_PROTOCOL+x} ]; then
	KONG_ADMIN_PROTOCOL=$KONG_DEFAULT_ADMIN_PROTOCOL
fi
export KONG_ADMIN_PROTOCOL

# Verify if KONG_ADMIN_HOST exists, if not exists set default value
if [ -z ${KONG_ADMIN_HOST+x} ]; then
	KONG_ADMIN_HOST=$KONG_DEFAULT_ADMIN_HOST
fi
export KONG_ADMIN_HOST

# Verify if KONG_ADMIN_PORT exists, if not exists set default value
if [ -z ${KONG_ADMIN_PORT+x} ]; then
	KONG_ADMIN_PORT=$KONG_DEFAULT_ADMIN_PORT
fi
export KONG_ADMIN_PORT

DAEMON=kong-dashboard
ARGS="start --kong-url $KONG_ADMIN_PROTOCOL://$KONG_ADMIN_HOST:$KONG_ADMIN_PORT"

# Verify if process is already running
if [ -e /var/run/$DAEMON.pid ]; then
	# Kill process and remove PID file
	PID=`cat /var/run/$DAEMON.pid`
  rm /var/run/$DAEMON.pid
  kill $PID
fi

while ! nc -q 1 $KONG_ADMIN_HOST $KONG_ADMIN_PORT </dev/null; do
	echo "Waiting $KONG_ADMIN_HOST $KONG_ADMIN_PORT..."
	sleep 5
done

# Start process and save PID
nohup $DAEMON $ARGS 1>/var/log/$DAEMON.out 2>/var/log/$DAEMON.err &
echo $! > /var/run/$DAEMON.pid

exit 0
